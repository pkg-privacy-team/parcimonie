#!perl

use Test::Most tests => 1;

use strictures 2;
use lib qw{t/lib};
use Test::Utils;
use App::Parcimonie;

my $gnupg_options;

# with a keyserver defined in dirmngr.conf
my $gnupg_homedir = Test::Utils::new_temporary_GnuPG_homedir();

lives_ok { checkGpgHasDefinedKeyserver({ homedir => $gnupg_homedir }) }
    "checkGpgHasDefinedKeyserver detects the keyserver defined in dirmngr.conf";

Test::Utils::cleanup_temporary_GnuPG_homedir($gnupg_homedir);
