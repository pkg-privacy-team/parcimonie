#!perl -T

use Test::Most tests => 12;
use strictures 2;

my @set  = (0, 1, 2, 3);
my @list = (0, 1, 1, 3);

use App::Parcimonie;
use List::MoreUtils qw{uniq};

my @res;

### pickRandomItems

dies_ok { App::Parcimonie::pickRandomItems() } "pickRandomItems sends an exception if N is not defined";
dies_ok { App::Parcimonie::pickRandomItems("abc") } "pickRandomItems sends an exception if N is not an integer";
dies_ok { App::Parcimonie::pickRandomItems(42, @set) }
    "pickRandomItems sends an exception if N is too big";
dies_ok { App::Parcimonie::pickRandomItems(42, @list) }
    "pickRandomItems sends an exception if N is too big";
dies_ok { App::Parcimonie::pickRandomItems(42, ()) }
    "pickRandomItems sends an exception if N>0 and empty input list";

@res = App::Parcimonie::pickRandomItems(2, @set);
is(scalar(@res), 2,    "returns a list with N items");
is(uniq(@res),   @res, "returns a list of unique items");

@res = App::Parcimonie::pickRandomItems(2, @list);
is(scalar(@res), 2,    "returns a list with N items");
is(uniq(@res),   @res, "returns a list of unique items");

@res = App::Parcimonie::pickRandomItems(0, @set);
is(scalar(@res), 0, "returns the empty list when N=0");

@res = App::Parcimonie::pickRandomItems(0, @list);
is(scalar(@res), 0, "returns the empty list when N=0");

@res = App::Parcimonie::pickRandomItems(0, ());
is(scalar(@res), 0, "returns the empty list when N=0 and empty input list");
