
BEGIN {
  unless ($ENV{AUTHOR_TESTING}) {
    print qq{1..0 # SKIP these tests are for testing by the author\n};
    exit
  }
}

use strict;
use warnings;

# this test was generated with Dist::Zilla::Plugin::Test::EOL 0.19

use Test::More 0.88;
use Test::EOL;

my @files = (
    'bin/parcimonie',
    'lib/App/Parcimonie.pm',
    'lib/App/Parcimonie/DBus/Object.pm',
    'lib/App/Parcimonie/Daemon.pm',
    'lib/App/Parcimonie/GnuPG/Interface.pm',
    'lib/App/Parcimonie/Role/HasCodeset.pm',
    'lib/App/Parcimonie/Role/HasEncoding.pm',
    't/00-load_all.t',
    't/00-report-prereqs.dd',
    't/00-report-prereqs.t',
    't/01-check_gpg_version.t',
    't/30-pickRandomItems.t',
    't/31-gpgPublicKeys.t',
    't/32-keyserver_defined_on_command_line.t',
    't/33-checkGpgHasDefinedKeyserver.t',
    't/90-tryRecvKey.t',
    't/91-gpgRecvKeys.t',
    't/author-critic.t',
    't/author-eol.t',
    't/author-no-tabs.t',
    't/author-pod-syntax.t',
    't/data/gnupg_homedir/dirmngr.conf',
    't/data/gnupg_homedir/dirmngr_ldapservers.conf',
    't/data/pubkeys/intrigeri.asc',
    't/data/pubkeys/rms.asc',
    't/lib/Test/Utils.pm',
    't/release-kwalitee.t'
);

eol_unix_ok($_, { trailing_whitespace => 1 }) foreach @files;
done_testing;
