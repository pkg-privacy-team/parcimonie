#!perl

use strictures 2;

BEGIN {
  unless ($ENV{RELEASE_TESTING}) {
    require Test::More;
    Test::More::plan(skip_all => 'these tests are for release candidate testing');
  }
}

use LWP::Online ':skip_all';
use Test::Most tests => 2;
use Test::Trap;
use lib qw{t/lib};
use Test::Utils;

my $nonexistent_keyid = 'A'x40;
my $existent_keyid    = '6F818B215E159EF3FA26B0BE624DC565135EA668';
my $gnupg_homedir     = Test::Utils::new_temporary_GnuPG_homedir();

$ENV{LC_ALL} = 'C';

use App::Parcimonie::Daemon;

my $daemon = App::Parcimonie::Daemon->new_with_options(
    gnupg_homedir => $gnupg_homedir->stringify
);

trap { $daemon->tryRecvKey( $nonexistent_keyid ) };
is ( $trap->stderr, '',
     'No STDERR when failing to receive a non-existing key.' );
trap { $daemon->tryRecvKey( $existent_keyid ) };
is ( $trap->stderr, '', 'No STDERR when trying to receive an existing key.' );

Test::Utils::cleanup_temporary_GnuPG_homedir($gnupg_homedir);
