#!perl

use Test::Most tests => 4;

use strictures 2;
use App::Parcimonie::Daemon;
use Net::DBus::Reactor;
use lib qw{t/lib};
use Test::Utils;

my @tests = (
    {
        should_succeed   => 0,
        gnupg_extra_args => [],
        explanation      => 'no keyserver is set on the command line',
    },
    {
        should_succeed   => 0,
        gnupg_extra_args => ['--keyserver'],
        explanation      => '--keyserver lacks its argument',
    },
    {
        should_succeed   => 1,
        gnupg_extra_args => [ '--keyserver=hkp://example.com' ],
        explanation      => '--keyserver followed by equal sign and something',
    },
    {
        should_succeed   => 1,
        gnupg_extra_args => [ '--keyserver hkp://example.com' ],
        explanation      => '--keyserver followed by space and something',
    },
);

sub run {
    my $test = shift;
    my $gnupg_homedir = Test::Utils::new_temporary_GnuPG_homedir();
    my $daemon = App::Parcimonie::Daemon->new_with_options(
        gnupg_extra_args => $test->{gnupg_extra_args},
        gnupg_homedir    => $gnupg_homedir,
    );
    my $res = $daemon->keyserver_defined_on_command_line;
    Test::Utils::cleanup_temporary_GnuPG_homedir($gnupg_homedir);
    $test->{should_succeed} ? $res : !$res;
}

foreach my $test (@tests) {
    ok(run($test), $test->{explanation});
}
