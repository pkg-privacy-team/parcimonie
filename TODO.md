* Bugs and TODO items in the Debian bug tracker:
  https://bugs.debian.org/cgi-bin/pkgreport.cgi?src=parcimonie

* Robustness++

  - Don't wait indefinitely for spawned gpg processes. Somehow timeout.

* User feedback: see `user_interface.md`.

* Add functional tests, probably using Test::Trap.

* Make sure an update is attempted for every key on a regular basis.
  Remember last time a given key update has been attempted, and randomly
  pick the key to update among the oldest-updated keys. Beware: key update
  attempt time shall be recorded instead of successful key update time,
  else there is a risk to loop trying to update keys that are not available
  on the keyserver.

* Improve tests coverage

  - App::Parcimonie::Role::HasEncoding and
    App::Parcimonie::Role::HasCodeset could be quite easily tested

* Test suite: configure, use and run Tor
