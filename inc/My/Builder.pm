package My::Builder;

use strict;
use warnings;
use 5.10.0;

use base qw{Module::Build};

use autodie;
use Cwd;
use Path::Tiny;


=head1 Methods and method modifiers

=head2 ACTION_test

Set strict permissions on GnuPG test home directories.

=cut
sub ACTION_test {
    my $self = shift;
    my @args = @_;

    my $datadir = path(getcwd)->child('t', 'data');
    my $gnupg_homedir = $datadir->child('gnupg_homedir')->stringify;
    chmod 0700, $gnupg_homedir;

    $self->SUPER::ACTION_test(@args);
};

1;
