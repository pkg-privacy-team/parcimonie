Download
========

 - Git: see https://salsa.debian.org/intrigeri/parcimonie
 - [Released tarballs](https://gaffer.boum.org/intrigeri/files/parcimonie/)

Installation
============

Debian and derivative distributions
-----------------------------------

parcimonie is [in Debian](https://tracker.debian.org/pkg/parcimonie) Wheezy
and newer.

Standalone
----------

To manually install parcimonie, run the following commands:

	dzil test
	dzil build
	cd App-Parcimonie-*
	./Build
	./Build install


Support and Documentation
=========================

After installing, you can find documentation for parcimonie with the
man command:

    man parcimonie

parcimonie's homepage: https://salsa.debian.org/intrigeri/parcimonie
